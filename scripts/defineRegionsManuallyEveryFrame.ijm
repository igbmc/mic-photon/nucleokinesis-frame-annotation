// This macro allows for semi-automatic annotation of front and rear regions of a nucleus in a tracked stack obtained from TrackMate
// INSTRUCTIONS:
// 1. Open the stack you want to annotate, select it then run the macro
// 2. Move or Redraw the first circle, then press T to add it to ROI Manager -> the macro will rename the first one as the REAR annotation
// 3. Move or Redraw the second circle, press T -> the macro will rename it as the front annotation
// The macro will keep track of the frame in which the circles were added and rename the annotation with the frame number
// 4. The macro will then advance to the next frame and allows for continue annotations
// 5. You can use the scroll wheel to move around the stack if you want to skip frames, the macro will keep track of the current frame and rename the ROIs accordingly
// 6. Please press Escape when you want to stop annotating. The Macro will NOT stop by itself.

// AUTHOR:
// Marco Dalla Vecchia dallavem@igbmc.fr

// MAIN MACRO BODY
// Have the image you want to annotate selected
imgName = getTitle();
// Reset ROI Manager so it's empty at the start
roiManager("reset");
// Create container for X and Y coordinate of rear and front circles
circleCoords = newArray(100,100,110,110);
circleRadius = 5 // change this value if you want a bigger circle

// In this infinite loop keep annotating --> Press Escape to stop the loop
while (true) {
	defineRegionsAdvanceFrame(imgName, circleCoords, circleRadius); // for each loop define the regions and advance to next frame
}


// FUNCTIONS DEFINITION
function addCircleToROIManager(imgName, x, y, r, roiNameSuffix) { 
	// This function create a circle at x, y with radius r in the imgName Image
	// Wait for the user to press T to add the circle ROI to the ROI Manager
	// when the ROI was added i.e. the number of ROIs has increased, it renames it with frame number and roiNameSuffix
	
	selectImage(imgName);
	initialNumberROIs = roiManager("count");
	makeOval(x, y, r, r);
	while (initialNumberROIs == roiManager("count")) {
	    wait(100);
	}
	
	currentSliceIndex = getSliceNumber();
	nROIs = roiManager("count");
	roiManager("select", nROIs - 1);
	roiManager("rename", currentSliceIndex + "_" + roiNameSuffix);
}

function updateCircleCoordsFromLastTwoROIs(circleCoords){
	// This function updates the coordinates of the last two added circles in circleCoords to allow for easier annotations
	
	nROIs = roiManager("count");
	roiManager("select", nROIs - 2);
	Roi.getBounds(x1, y1, w, h);
	circleCoords[0] = x1;
	circleCoords[1] = y1;
	
	roiManager("select", nROIs - 1);
	Roi.getBounds(x2, y2, w, h);	
	circleCoords[2] = x2;
	circleCoords[3] = y2;	
}


function defineRegionsAdvanceFrame(imgName, circleCoords, circleRadius) { 
	// This is the main function to allow for annotation of the 2 circles, renaming them and advancing to the next frame
	
	setTool("oval");

	addCircleToROIManager(imgName, circleCoords[0], circleCoords[1], circleRadius, "rear");
	addCircleToROIManager(imgName, circleCoords[2], circleCoords[3], circleRadius, "front");
	
	updateCircleCoordsFromLastTwoROIs(circleCoords);
	
	currentSliceIndex = getSliceNumber();
	selectImage(imgName);
	setSlice(currentSliceIndex + 1); 

}
